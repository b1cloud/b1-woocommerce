module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        makepot: {
            target: {
                options: {
                    cwd: 'b1-accounting',
                    domainPath: '/languages',
                    potFilename: 'b1-accounting.pot',
                    type: 'wp-plugin',
                    updatePoFiles: true
                }
            }
        },
        po2mo: {
            files: {
                src: 'b1-accounting/languages/*.po',
                expand: true
            }
        }
    });

    grunt.registerTask('default', ['makepot', 'po2mo']);
};