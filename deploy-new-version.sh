#!/bin/bash

##########
# Deploy new version
##########

set -e # Exit on error
#set -x # See what is happening when executing the script.

# Make sure user specifies the new version
if [ $# -ne 1 ]; then
	echo "Please specify the new version."
	echo "Example: ./deploy-new-version.sh 2.0.0"
    exit 1
fi

git pull
sed -i "s/const VERSION = '.*';/const VERSION = '$1';/" src/b1-accounting/includes/lib/B1.php
sed -i "s/const PLATFORM = '.*';/const PLATFORM = 'woocommerce';/" src/b1-accounting/includes/lib/B1.php
sed -i "s/Version: .*/Version: $1/" src/b1-accounting/b1-accounting.php
sed -i "s/define('B1_ACCOUNTING_VERSION', '.*');/define('B1_ACCOUNTING_VERSION', '$1');/" src/b1-accounting/b1-accounting.php
sed -i "s/\"version\": \".*\",/\"version\": \"$1\",/" package.json
git commit -am "Bump plugin version"
git push
git tag -a $1 -m "New version"
git push --tags

cp -a src/b1-accounting/* svn/b1-accounting/trunk/
cd svn/b1-accounting
svn add trunk/* --force
svn ci -m "New version"
svn cp trunk tags/$1
svn ci -m "Bump plugin version"
cd ../../
